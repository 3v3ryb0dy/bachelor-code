------------------------------------------------------------------------------
RESULTS FOR THE UNIFORMITY OF P-VALUES AND THE PROPORTION OF PASSING SEQUENCES
------------------------------------------------------------------------------
   generator is </home/lab/bytes/urandom_lab1_100.dat>
------------------------------------------------------------------------------
 C1  C2  C3  C4  C5  C6  C7  C8  C9 C10  P-VALUE  PROPORTION  STATISTICAL TEST
------------------------------------------------------------------------------
109 111 112 113  95 120  87 110  93 123  0.229023   1066/1073    Frequency
107 106 103 124 112 113 106 110 106  86  0.569334   1067/1073    BlockFrequency
108 114 113  94 109  99 114 102 107 113  0.908310   1064/1073    CumulativeSums
104 114 109 115 108  94 103  97 115 114  0.842378   1064/1073    CumulativeSums
101 120 115  86 106 113 108 100 122 102  0.368166   1057/1073    Runs
116  94 117  92 122 116 113 109  91 103  0.265598   1058/1073    LongestRun
113 120 107  90 102 117 118 107 114  85  0.219167   1060/1073    Rank
106 105 114 107 103  90 111 125 112 100  0.613779   1066/1073    FFT
131 101 111  96 119 100 111 102 103  99  0.355807   1065/1073    NonOverlappingTemplate
110 116 110 113 102 107 102 104 108 101  0.988699   1059/1073    NonOverlappingTemplate
107 117 108 113  91 112 127  99  99 100  0.410138   1060/1073    NonOverlappingTemplate
108 107 102 112 101 106 109 117 106 105  0.993738   1060/1073    NonOverlappingTemplate
108 116 112 113  91 102 108 110 107 106  0.907054   1059/1073    NonOverlappingTemplate
103  89 113  99 100 100 117 117 120 115  0.430401   1062/1073    NonOverlappingTemplate
126 109 105 108  98  87  98 110 109 123  0.260647   1052/1073    NonOverlappingTemplate
127  95  97  96 112 109 108 108  96 125  0.236924   1059/1073    NonOverlappingTemplate
 94  93  99 109 133 112 110 117 109  97  0.182113   1066/1073    NonOverlappingTemplate
110 115 116 104 101 118  99 109 102  99  0.878776   1059/1073    NonOverlappingTemplate
112  97 105 102  96 107 113 108 114 119  0.858032   1063/1073    NonOverlappingTemplate
100 133  97 110 104 106  94 105 110 114  0.346716   1063/1073    NonOverlappingTemplate
105 111 109 116 101 103 111  99 106 112  0.981993   1066/1073    NonOverlappingTemplate
122 117  93  95  98  93 114 127 108 106  0.161762   1051/1073 *  NonOverlappingTemplate
101 118 114  87 110 110 113  99 122  99  0.395303   1067/1073    NonOverlappingTemplate
102 115  94 122 106 100 110 115  86 123  0.203483   1059/1073    NonOverlappingTemplate
106 104 120  98 111 109 114 118 106  87  0.538862   1061/1073    NonOverlappingTemplate
120 110 100  95 119  96 116 100 102 115  0.535088   1059/1073    NonOverlappingTemplate
109 107  96  99 102  92 126 120 106 116  0.365051   1068/1073    NonOverlappingTemplate
118  95  98 116  98 102 115 111 114 106  0.720126   1057/1073    NonOverlappingTemplate
 92 106 114  96  96 108 117 106  98 140  0.053292   1064/1073    NonOverlappingTemplate
107  93 107 107 102 103 115 105 122 112  0.817719   1055/1073    NonOverlappingTemplate
115 108 108  95 101 115 112 101 112 106  0.929426   1068/1073    NonOverlappingTemplate
107 117  88 107 117 120 107 114  84 112  0.188706   1061/1073    NonOverlappingTemplate
105  96 124 102 102 105 104 119 109 107  0.755708   1069/1073    NonOverlappingTemplate
112 121  96 116  89 101 104 104 128 102  0.222415   1065/1073    NonOverlappingTemplate
100 106  81 112 108 118 102 112 110 124  0.248571   1059/1073    NonOverlappingTemplate
108 106 109 119  87 122 114 107 103  98  0.474267   1063/1073    NonOverlappingTemplate
111  97 115 101 111  93 125 122 102  96  0.311892   1060/1073    NonOverlappingTemplate
 99 110 112 104 110 113 105 111  88 121  0.656573   1060/1073    NonOverlappingTemplate
 96  97 116 117 101  95 116 103 109 123  0.458231   1069/1073    NonOverlappingTemplate
 91 118  92 104 123  95 129 101 103 117  0.080703   1062/1073    NonOverlappingTemplate
101 106 101 118 113 106 106 112  93 117  0.819404   1062/1073    NonOverlappingTemplate
 97  94 101 118 118  96 125 100 125  99  0.147184   1064/1073    NonOverlappingTemplate
138 103  99 111  99  95 106 113 104 105  0.197465   1058/1073    NonOverlappingTemplate
106 106 115 105  93  99 126 112 115  96  0.492381   1059/1073    NonOverlappingTemplate
116 116  96 104 102  98 123 101 101 116  0.580870   1057/1073    NonOverlappingTemplate
110 116 104 101 104 110 110 111 102 105  0.992460   1059/1073    NonOverlappingTemplate
102 106 116 105  97 109 114 116 106 102  0.942863   1064/1073    NonOverlappingTemplate
 94 109 102 131 107 114 110 105 107  94  0.401858   1062/1073    NonOverlappingTemplate
115 121 101  97  91 120 108 121  99 100  0.306331   1063/1073    NonOverlappingTemplate
111 110 105  98 107 111 110 114 107 100  0.988338   1063/1073    NonOverlappingTemplate
131 103 109  95 113 115 106  85  96 120  0.088401   1061/1073    NonOverlappingTemplate
 99 112 100 110  99 112 111  98 119 113  0.851851   1064/1073    NonOverlappingTemplate
102 119 120 118 107  98 101 103 107  98  0.710608   1062/1073    NonOverlappingTemplate
107 113  98 125 109 107 114 106  91 103  0.619610   1060/1073    NonOverlappingTemplate
 91  98  86 107 114 111 101 128 122 115  0.089914   1065/1073    NonOverlappingTemplate
117  99 104 115 106  95 109 115  96 117  0.712516   1063/1073    NonOverlappingTemplate
130 107 119  89 103 113 106  90 110 106  0.181187   1061/1073    NonOverlappingTemplate
111 124  98 106  99 105 109 105 106 110  0.881620   1059/1073    NonOverlappingTemplate
 97 104 105 106  91 135 123 101 100 111  0.118069   1063/1073    NonOverlappingTemplate
118 121  95 118 105  97  89 120 103 107  0.264354   1066/1073    NonOverlappingTemplate
 94 110 109 112 105 119 103  98 109 114  0.858032   1064/1073    NonOverlappingTemplate
 97  93  96 116 113 107 118 112 110 111  0.679846   1068/1073    NonOverlappingTemplate
 96 106  98 114 113 110  99 116 116 105  0.840776   1064/1073    NonOverlappingTemplate
 99 118 117 112  90 107 105 120  93 112  0.411806   1065/1073    NonOverlappingTemplate
107 111 111 102 102 103 105  92 108 132  0.445946   1063/1073    NonOverlappingTemplate
127 106 111  95 102 115 110 102 112  93  0.488734   1055/1073    NonOverlappingTemplate
120  97 114 110  87 109 119 109  94 114  0.330446   1062/1073    NonOverlappingTemplate
106 115 105 127  87 106 100  98 104 125  0.188706   1066/1073    NonOverlappingTemplate
103  88 125 118  96 101 112 124 117  89  0.065912   1060/1073    NonOverlappingTemplate
113  99 103 107 103 114  98 128  96 112  0.540752   1061/1073    NonOverlappingTemplate
111  99 112 120 116 111  97 102  96 109  0.759389   1061/1073    NonOverlappingTemplate
128 118 107 100 103 100 109 113  89 106  0.372869   1062/1073    NonOverlappingTemplate
127  92 111 102 123 110  97 108 108  95  0.275709   1057/1073    NonOverlappingTemplate
118 105  94 114  95 110 126 111  98 102  0.423587   1068/1073    NonOverlappingTemplate
111 109 106  98 106 100 116 112 117  98  0.904517   1061/1073    NonOverlappingTemplate
110 112 129 108 108 112 102  90 107  95  0.403506   1064/1073    NonOverlappingTemplate
 87  94 114 118 106 121 121 101 108 103  0.270619   1060/1073    NonOverlappingTemplate
101 114 118 120 101 123 108  99  93  96  0.366606   1059/1073    NonOverlappingTemplate
103 116 121  94  97 113 122 100 102 105  0.492381   1066/1073    NonOverlappingTemplate
 98 109 108 103  94 121 118 101 109 112  0.729587   1065/1073    NonOverlappingTemplate
118 108 116  97 116  98  94 101 108 117  0.615722   1062/1073    NonOverlappingTemplate
112 104  89 134 122 105 111  97  96 103  0.100602   1060/1073    NonOverlappingTemplate
108  94 107 122 103 114  98 116 112  99  0.674040   1058/1073    NonOverlappingTemplate
109  90 100 106 108 118 118 118 105 101  0.639066   1063/1073    NonOverlappingTemplate
132  98 102 121  97 113 113  94  88 115  0.070684   1059/1073    NonOverlappingTemplate
113 103  97  95 122  86 106 115 123 113  0.195491   1066/1073    NonOverlappingTemplate
105 109 132 112 100 100 111 105 108  91  0.379197   1060/1073    NonOverlappingTemplate
 89 115 110 101 104 106 103 121 117 107  0.631281   1065/1073    NonOverlappingTemplate
121 102 107 109 106 104 107  91 125 101  0.538862   1062/1073    NonOverlappingTemplate
 87 106 117 110  96 102 129 108 102 116  0.233513   1062/1073    NonOverlappingTemplate
135  98 102 110 116 114  93  82 111 112  0.039164   1062/1073    NonOverlappingTemplate
 91 101 101 113  99  92 115 121 135 105  0.069060   1066/1073    NonOverlappingTemplate
 92 134 103  95 116  96  97 110 109 121  0.085928   1067/1073    NonOverlappingTemplate
102  98  90 100 104 110 122  94 126 127  0.087901   1064/1073    NonOverlappingTemplate
106 112 113 102 120  97 103 100 112 108  0.899341   1061/1073    NonOverlappingTemplate
108 101 104 115 126  95 107 109 117  91  0.426987   1060/1073    NonOverlappingTemplate
123  98  94 111 105 117 111  94 121  99  0.349729   1063/1073    NonOverlappingTemplate
109 106  88 105 111 119  96 121 115 103  0.468893   1052/1073    NonOverlappingTemplate
109 108 108 108  92 106 108 136  88 110  0.141068   1066/1073    NonOverlappingTemplate
103 109 127 101 100  99 109 117 117  91  0.382386   1066/1073    NonOverlappingTemplate
126  96 112 132 101 102  99 107  85 113  0.055230   1060/1073    NonOverlappingTemplate
 97 107  90 112 120 104 118 101 107 117  0.540752   1067/1073    NonOverlappingTemplate
 94 111 100  96 124 113 109 121  89 116  0.212783   1061/1073    NonOverlappingTemplate
108 113  91 124 102  98 121 101 104 111  0.452946   1063/1073    NonOverlappingTemplate
101 116 102 118  92 119 105  94 117 109  0.485099   1062/1073    NonOverlappingTemplate
102 114  89 108 107 112 110 106 105 120  0.772157   1063/1073    NonOverlappingTemplate
101 119 116 100 117 106  90 101 108 115  0.586656   1062/1073    NonOverlappingTemplate
119 100 102 106 108  93 126 100 109 110  0.550233   1067/1073    NonOverlappingTemplate
 98 105 110 102 104 110 100  99 122 123  0.654629   1061/1073    NonOverlappingTemplate
110 109 111  97  96 122 111 112 108  97  0.763056   1063/1073    NonOverlappingTemplate
120  97  96 105 101  94 104 125 120 111  0.316109   1066/1073    NonOverlappingTemplate
 99 107  97 105 109 111 112 106 108 119  0.948537   1062/1073    NonOverlappingTemplate
122 128  99 103 103 101 125  84 103 105  0.075327   1063/1073    NonOverlappingTemplate
 98 122 116 112  98 113 118 104  95  97  0.490556   1058/1073    NonOverlappingTemplate
 99 106 117  94 111 103 103 112 116 112  0.851851   1064/1073    NonOverlappingTemplate
102 119 111 109 112  97 104 108 106 105  0.961257   1058/1073    NonOverlappingTemplate
 97  90  99 117 106 118  96 122 125 103  0.175709   1065/1073    NonOverlappingTemplate
123  96 116 124 107 104  97  95 118  93  0.196476   1061/1073    NonOverlappingTemplate
102 123  96 106 103 119  98 110 114 102  0.658516   1056/1073    NonOverlappingTemplate
104  98 102 104 118 114 126 110  99  98  0.578944   1061/1073    NonOverlappingTemplate
111 104 101 103  94 102 104 117 118 119  0.737109   1067/1073    NonOverlappingTemplate
110 109 121 108 121 105 100  91 107 101  0.635173   1061/1073    NonOverlappingTemplate
107 107 104 113 105 111 107 114 106  99  0.995649   1060/1073    NonOverlappingTemplate
 95 118 110 109 116 101 102  90 106 126  0.349729   1061/1073    NonOverlappingTemplate
121 100 110 119 117 106  86 108 104 102  0.426987   1057/1073    NonOverlappingTemplate
122  97 104  97 117  96 118 109  98 115  0.470681   1057/1073    NonOverlappingTemplate
119  99  88 108 116 113  87 113 110 120  0.204500   1063/1073    NonOverlappingTemplate
106 101  99 117 112 115 110 113  94 106  0.858032   1065/1073    NonOverlappingTemplate
122  99  99 110 100 106 105 118 102 112  0.790055   1057/1073    NonOverlappingTemplate
113 123 101 100 102 114 108 110  86 116  0.420202   1063/1073    NonOverlappingTemplate
 91 106 109 119 110 116 106  98 115 103  0.725810   1060/1073    NonOverlappingTemplate
108  85 101 105 113 128 105 118 112  98  0.241532   1066/1073    NonOverlappingTemplate
120 106 123  96 111 108 101 100 114  94  0.523820   1060/1073    NonOverlappingTemplate
113  98 114 109 107  87 127 104 105 109  0.416832   1062/1073    NonOverlappingTemplate
108 104 109 105  98 122  99  86 117 125  0.230139   1056/1073    NonOverlappingTemplate
100 115 104  98 123 109 112 101 108 103  0.829393   1065/1073    NonOverlappingTemplate
126 124 100 119  99 100  96 100 107 102  0.292733   1059/1073    NonOverlappingTemplate
105 109 120  93 121  92  98 119 113 103  0.349729   1063/1073    NonOverlappingTemplate
113 107 101 105 108 117  91 120 117  94  0.520083   1061/1073    NonOverlappingTemplate
116 105 109 120 125  99  85 111  92 111  0.164305   1058/1073    NonOverlappingTemplate
109 118 112 100 117  99 105 111 101 101  0.894033   1058/1073    NonOverlappingTemplate
112 101  98 107 102 111 108 118  96 120  0.782947   1064/1073    NonOverlappingTemplate
 95 118 112  93 122  83 116 103 106 125  0.067469   1066/1073    NonOverlappingTemplate
 89 110 109 105  96 117  97 112 120 118  0.425285   1066/1073    NonOverlappingTemplate
111  94 102 109 107 118 115 101 120  96  0.660459   1065/1073    NonOverlappingTemplate
100 111 121 125 113 121  97 101  97  87  0.138091   1059/1073    NonOverlappingTemplate
107 118 109 113 115  94 106 107  85 119  0.387200   1060/1073    NonOverlappingTemplate
110 102 110 104 121 127 106  95  94 104  0.430401   1057/1073    NonOverlappingTemplate
102 114 100  96 112 105 107 109 121 107  0.881620   1065/1073    NonOverlappingTemplate
107 102  90  95 129 108 115 119  97 111  0.222415   1067/1073    NonOverlappingTemplate
131 101 111  95 120 100 111 101 103 100  0.323223   1065/1073    NonOverlappingTemplate
 97 110 112 115 103 102  96 110 120 108  0.831037   1063/1073    NonOverlappingTemplate
109  92  94 126 111 112 124  92 102 111  0.178430   1068/1073    NonOverlappingTemplate
114 127  98 114 116 101  98  95  93 117  0.250952   1059/1073    NonOverlappingTemplate
103 100 119 106 103  89 123 103 116 111  0.476065   1059/1073    NonOverlappingTemplate
110 118 113 107 115 108 104  98  94 106  0.868582   1064/1073    NonOverlappingTemplate
108 105 108 101 101 106 112 123 106 103  0.939900   1053/1073    OverlappingTemplate
128 113  92 110 100 100 113 104 105 108  0.531323   1060/1073    Universal
101 105 105  96 106 114 104 114 104 124  0.804049   1063/1073    ApproximateEntropy
 64  72  67  63  64  63  63  71  71  72  0.987716    664/670     RandomExcursions
 81  66  80  65  56  74  51  61  76  60  0.110141    665/670     RandomExcursions
 71  77  52  83  64  64  75  58  69  57  0.167391    663/670     RandomExcursions
 66  74  71  72  64  66  68  52  71  66  0.822445    667/670     RandomExcursions
 55  86  65  59  82  65  62  72  45  79  0.008599    664/670     RandomExcursions
 59  75  73  56  60  73  75  74  56  69  0.430414    668/670     RandomExcursions
 74  59  68  64  63  60  69  68  65  80  0.792255    668/670     RandomExcursions
 65  56  67  67  73  71  66  57  83  65  0.531141    667/670     RandomExcursions
 68  60  79  85  50  64  71  77  65  51  0.037677    666/670     RandomExcursionsVariant
 59  74  72  77  64  72  65  52  79  56  0.247202    666/670     RandomExcursionsVariant
 64  77  71  73  65  70  61  65  54  70  0.754758    666/670     RandomExcursionsVariant
 74  68  77  70  50  71  71  53  71  65  0.319992    665/670     RandomExcursionsVariant
 65  79  69  64  70  61  71  54  59  78  0.475273    663/670     RandomExcursionsVariant
 65  74  53  72  80  70  60  58  75  63  0.360238    663/670     RandomExcursionsVariant
 62  74  79  56  58  73  72  55  77  64  0.278791    662/670     RandomExcursionsVariant
 70  63  62  63  69  71  79  62  59  72  0.822445    662/670     RandomExcursionsVariant
 74  62  73  70  69  57  60  64  57  84  0.350485    662/670     RandomExcursionsVariant
 73  63  59  65  68  78  69  76  55  64  0.620031    662/670     RandomExcursionsVariant
 59  67  65  63  67  66  73  71  70  69  0.987104    662/670     RandomExcursionsVariant
 55  54  75  64  75  83  71  46  79  68  0.021100    663/670     RandomExcursionsVariant
 59  58  65  62  82  65  69  71  74  65  0.626243    666/670     RandomExcursionsVariant
 57  57  67  71  67  63  70  55  79  84  0.201784    666/670     RandomExcursionsVariant
 53  63  76  69  53  58  72  84  62  80  0.063829    664/670     RandomExcursionsVariant
 55  62  64  70  62  84  67  68  61  77  0.390514    665/670     RandomExcursionsVariant
 58  60  58  60  76  78  81  72  64  63  0.306547    666/670     RandomExcursionsVariant
 57  56  64  68  75  71  75  57  71  76  0.481031    666/670     RandomExcursionsVariant
109 108  94 107 101 108 107 114 111 114  0.962794   1054/1073    Serial
111  90 108 101 112  94 113 107 112 125  0.474267   1063/1073    Serial
117 120  93 106 111 116 102 103 111  94  0.598259   1067/1073    LinearComplexity


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
The minimum pass rate for each statistical test with the exception of the
random excursion (variant) test is approximately = 1052 for a
sample size = 1073 binary sequences.

The minimum pass rate for the random excursion (variant) test
is approximately = 655 for a sample size = 670 binary sequences.

For further guidelines construct a probability table using the MAPLE program
provided in the addendum section of the documentation.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -