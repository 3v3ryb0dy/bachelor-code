#=============================================================================#
#            dieharder version 3.31.1 Copyright 2003 Robert G. Brown          #
#=============================================================================#
   rng_name    |           filename             |rands/second|
 file_input_raw|/home/lab/bytes/lcg/resources/lcg_mod16777216_a189_c1_10G.dat|  3.18e+07  |
#=============================================================================#
        test_name   |ntup| tsamples |psamples|  p-value |Assessment
#=============================================================================#
   diehard_birthdays|   0|       100|     100|0.05975288|  PASSED  
      diehard_operm5|   0|   1000000|     100|0.00000000|  FAILED  
  diehard_rank_32x32|   0|     40000|     100|0.00000000|  FAILED  
    diehard_rank_6x8|   0|    100000|     100|0.00000022|  FAILED  
   diehard_bitstream|   0|   2097152|     100|0.00000000|  FAILED  
        diehard_opso|   0|   2097152|     100|0.00000000|  FAILED  
        diehard_oqso|   0|   2097152|     100|0.00000000|  FAILED  
         diehard_dna|   0|   2097152|     100|0.00000000|  FAILED  
diehard_count_1s_str|   0|    256000|     100|0.98623253|  PASSED  
diehard_count_1s_byt|   0|    256000|     100|0.51443202|  PASSED  
 diehard_parking_lot|   0|     12000|     100|0.00478323|   WEAK   
    diehard_2dsphere|   2|      8000|     100|0.00000000|  FAILED  
    diehard_3dsphere|   3|      4000|     100|0.00000000|  FAILED  
     diehard_squeeze|   0|    100000|     100|0.00298691|   WEAK   
        diehard_sums|   0|       100|     100|0.11710070|  PASSED  
        diehard_runs|   0|    100000|     100|0.00000000|  FAILED  
        diehard_runs|   0|    100000|     100|0.12892395|  PASSED  
       diehard_craps|   0|    200000|     100|0.20294690|  PASSED  
       diehard_craps|   0|    200000|     100|0.00000000|  FAILED  
# The file file_input_raw was rewound 1 times
 marsaglia_tsang_gcd|   0|  10000000|     100|0.00000000|  FAILED  
 marsaglia_tsang_gcd|   0|  10000000|     100|0.00000000|  FAILED  
# The file file_input_raw was rewound 1 times
         sts_monobit|   1|    100000|     100|0.82235336|  PASSED  
# The file file_input_raw was rewound 1 times
            sts_runs|   2|    100000|     100|0.44810016|  PASSED  
# The file file_input_raw was rewound 1 times
          sts_serial|   1|    100000|     100|0.16718648|  PASSED  
          sts_serial|   2|    100000|     100|0.00505463|  PASSED  
          sts_serial|   3|    100000|     100|0.00274069|   WEAK   
          sts_serial|   3|    100000|     100|0.18351457|  PASSED  
          sts_serial|   4|    100000|     100|0.89452732|  PASSED  
          sts_serial|   4|    100000|     100|0.00226621|   WEAK   
          sts_serial|   5|    100000|     100|0.42257892|  PASSED  
          sts_serial|   5|    100000|     100|0.05380019|  PASSED  
          sts_serial|   6|    100000|     100|0.36017440|  PASSED  
          sts_serial|   6|    100000|     100|0.75022055|  PASSED  
          sts_serial|   7|    100000|     100|0.81224560|  PASSED  
          sts_serial|   7|    100000|     100|0.01054251|  PASSED  
          sts_serial|   8|    100000|     100|0.00476556|   WEAK   
          sts_serial|   8|    100000|     100|0.00780111|  PASSED  
          sts_serial|   9|    100000|     100|0.00000000|  FAILED  
          sts_serial|   9|    100000|     100|0.00000000|  FAILED  
          sts_serial|  10|    100000|     100|0.00000000|  FAILED  
          sts_serial|  10|    100000|     100|0.00000000|  FAILED  
          sts_serial|  11|    100000|     100|0.00000000|  FAILED  
          sts_serial|  11|    100000|     100|0.00000000|  FAILED  
          sts_serial|  12|    100000|     100|0.00000000|  FAILED  
          sts_serial|  12|    100000|     100|0.00000000|  FAILED  
          sts_serial|  13|    100000|     100|0.00000000|  FAILED  
          sts_serial|  13|    100000|     100|0.00000000|  FAILED  
          sts_serial|  14|    100000|     100|0.00000000|  FAILED  
          sts_serial|  14|    100000|     100|0.00000000|  FAILED  
          sts_serial|  15|    100000|     100|0.00000000|  FAILED  
          sts_serial|  15|    100000|     100|0.00000000|  FAILED  
          sts_serial|  16|    100000|     100|0.00000000|  FAILED  
          sts_serial|  16|    100000|     100|0.00000000|  FAILED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   1|    100000|     100|0.00000001|  FAILED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   2|    100000|     100|0.00000000|  FAILED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   3|    100000|     100|0.62017394|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   4|    100000|     100|0.99755380|   WEAK   
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   5|    100000|     100|0.94269741|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   6|    100000|     100|0.75383226|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   7|    100000|     100|0.50696005|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   8|    100000|     100|0.05050235|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   9|    100000|     100|0.56712687|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|  10|    100000|     100|0.62997127|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|  11|    100000|     100|0.09993248|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|  12|    100000|     100|0.11201419|  PASSED  
# The file file_input_raw was rewound 1 times
rgb_minimum_distance|   2|     10000|    1000|0.00000000|  FAILED  
# The file file_input_raw was rewound 1 times
rgb_minimum_distance|   3|     10000|    1000|0.00000000|  FAILED  
# The file file_input_raw was rewound 1 times
rgb_minimum_distance|   4|     10000|    1000|0.00000000|  FAILED  
# The file file_input_raw was rewound 1 times
rgb_minimum_distance|   5|     10000|    1000|0.00000000|  FAILED  
# The file file_input_raw was rewound 1 times
    rgb_permutations|   2|    100000|     100|0.03717242|  PASSED  
# The file file_input_raw was rewound 1 times
    rgb_permutations|   3|    100000|     100|0.08026269|  PASSED  
# The file file_input_raw was rewound 1 times
    rgb_permutations|   4|    100000|     100|0.00000000|  FAILED  
# The file file_input_raw was rewound 1 times
    rgb_permutations|   5|    100000|     100|0.05893473|  PASSED  
# The file file_input_raw was rewound 1 times
      rgb_lagged_sum|   0|   1000000|     100|0.00005290|   WEAK   
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   1|   1000000|     100|0.00025957|   WEAK   
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   2|   1000000|     100|0.05417184|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   3|   1000000|     100|0.00000000|  FAILED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   4|   1000000|     100|0.83850850|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   5|   1000000|     100|0.00005674|   WEAK   
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   6|   1000000|     100|0.00001263|   WEAK   
# The file file_input_raw was rewound 3 times
      rgb_lagged_sum|   7|   1000000|     100|0.00000000|  FAILED  
# The file file_input_raw was rewound 3 times
      rgb_lagged_sum|   8|   1000000|     100|0.00033024|   WEAK   
# The file file_input_raw was rewound 3 times
      rgb_lagged_sum|   9|   1000000|     100|0.00081657|   WEAK   
# The file file_input_raw was rewound 4 times
      rgb_lagged_sum|  10|   1000000|     100|0.10677806|  PASSED  
# The file file_input_raw was rewound 4 times
      rgb_lagged_sum|  11|   1000000|     100|0.00000081|  FAILED  
# The file file_input_raw was rewound 5 times
      rgb_lagged_sum|  12|   1000000|     100|0.12429556|  PASSED  
# The file file_input_raw was rewound 5 times
      rgb_lagged_sum|  13|   1000000|     100|0.00001205|   WEAK   
# The file file_input_raw was rewound 6 times
      rgb_lagged_sum|  14|   1000000|     100|0.00128038|   WEAK   
# The file file_input_raw was rewound 6 times
      rgb_lagged_sum|  15|   1000000|     100|0.00000000|  FAILED  
# The file file_input_raw was rewound 7 times
      rgb_lagged_sum|  16|   1000000|     100|0.00005400|   WEAK   
# The file file_input_raw was rewound 8 times
      rgb_lagged_sum|  17|   1000000|     100|0.00000000|  FAILED  
# The file file_input_raw was rewound 8 times
      rgb_lagged_sum|  18|   1000000|     100|0.03138207|  PASSED  
# The file file_input_raw was rewound 9 times
      rgb_lagged_sum|  19|   1000000|     100|0.00000000|  FAILED  
# The file file_input_raw was rewound 10 times
      rgb_lagged_sum|  20|   1000000|     100|0.32057399|  PASSED  
# The file file_input_raw was rewound 11 times
      rgb_lagged_sum|  21|   1000000|     100|0.06178689|  PASSED  
# The file file_input_raw was rewound 12 times
      rgb_lagged_sum|  22|   1000000|     100|0.00000277|   WEAK   
# The file file_input_raw was rewound 13 times
      rgb_lagged_sum|  23|   1000000|     100|0.00003535|   WEAK   
# The file file_input_raw was rewound 14 times
      rgb_lagged_sum|  24|   1000000|     100|0.00033531|   WEAK   
# The file file_input_raw was rewound 14 times
      rgb_lagged_sum|  25|   1000000|     100|0.20864152|  PASSED  
# The file file_input_raw was rewound 15 times
      rgb_lagged_sum|  26|   1000000|     100|0.44025089|  PASSED  
# The file file_input_raw was rewound 17 times
      rgb_lagged_sum|  27|   1000000|     100|0.00000000|  FAILED  
# The file file_input_raw was rewound 18 times
      rgb_lagged_sum|  28|   1000000|     100|0.00935856|  PASSED  
# The file file_input_raw was rewound 19 times
      rgb_lagged_sum|  29|   1000000|     100|0.00007500|   WEAK   
# The file file_input_raw was rewound 20 times
      rgb_lagged_sum|  30|   1000000|     100|0.00248367|   WEAK   
# The file file_input_raw was rewound 21 times
      rgb_lagged_sum|  31|   1000000|     100|0.00000000|  FAILED  
# The file file_input_raw was rewound 22 times
      rgb_lagged_sum|  32|   1000000|     100|0.00004171|   WEAK   
# The file file_input_raw was rewound 22 times
     rgb_kstest_test|   0|     10000|    1000|0.53973101|  PASSED  
# The file file_input_raw was rewound 22 times
     dab_bytedistrib|   0|  51200000|       1|1.00000000|  FAILED  
# The file file_input_raw was rewound 22 times
             dab_dct| 256|     50000|       1|0.54204316|  PASSED  
# The file file_input_raw was rewound 22 times
Preparing to run test 207.  ntuple = 0
        dab_filltree|  32|  15000000|       1|0.00000000|  FAILED  
        dab_filltree|  32|  15000000|       1|0.00000000|  FAILED  
# The file file_input_raw was rewound 22 times
Preparing to run test 208.  ntuple = 0
       dab_filltree2|   0|   5000000|       1|0.00000000|  FAILED  
       dab_filltree2|   1|   5000000|       1|0.00000000|  FAILED  
# The file file_input_raw was rewound 22 times
Preparing to run test 209.  ntuple = 0
        dab_monobit2|  12|  65000000|       1|1.00000000|  FAILED  