#=============================================================================#
#            dieharder version 3.31.1 Copyright 2003 Robert G. Brown          #
#=============================================================================#
   rng_name    |           filename             |rands/second|
 file_input_raw|/home/lab/bytes/10G_conditioned_from_qrng|  3.69e+07  |
#=============================================================================#
        test_name   |ntup| tsamples |psamples|  p-value |Assessment
#=============================================================================#
   diehard_birthdays|   0|       100|     100|0.58538671|  PASSED  
      diehard_operm5|   0|   1000000|     100|0.16225970|  PASSED  
  diehard_rank_32x32|   0|     40000|     100|0.97635735|  PASSED  
    diehard_rank_6x8|   0|    100000|     100|0.20402639|  PASSED  
   diehard_bitstream|   0|   2097152|     100|0.84004272|  PASSED  
        diehard_opso|   0|   2097152|     100|0.96568569|  PASSED  
        diehard_oqso|   0|   2097152|     100|0.37910848|  PASSED  
         diehard_dna|   0|   2097152|     100|0.00568633|  PASSED  
diehard_count_1s_str|   0|    256000|     100|0.78447213|  PASSED  
diehard_count_1s_byt|   0|    256000|     100|0.61455963|  PASSED  
 diehard_parking_lot|   0|     12000|     100|0.71705096|  PASSED  
    diehard_2dsphere|   2|      8000|     100|0.25864676|  PASSED  
    diehard_3dsphere|   3|      4000|     100|0.27037488|  PASSED  
     diehard_squeeze|   0|    100000|     100|0.81969791|  PASSED  
        diehard_sums|   0|       100|     100|0.10703146|  PASSED  
        diehard_runs|   0|    100000|     100|0.20409074|  PASSED  
        diehard_runs|   0|    100000|     100|0.41739372|  PASSED  
       diehard_craps|   0|    200000|     100|0.96194561|  PASSED  
       diehard_craps|   0|    200000|     100|0.69723215|  PASSED  
# The file file_input_raw was rewound 1 times
 marsaglia_tsang_gcd|   0|  10000000|     100|0.99796605|   WEAK   
 marsaglia_tsang_gcd|   0|  10000000|     100|0.04318993|  PASSED  
# The file file_input_raw was rewound 1 times
         sts_monobit|   1|    100000|     100|0.45563392|  PASSED  
# The file file_input_raw was rewound 1 times
            sts_runs|   2|    100000|     100|0.87504161|  PASSED  
# The file file_input_raw was rewound 1 times
          sts_serial|   1|    100000|     100|0.57307926|  PASSED  
          sts_serial|   2|    100000|     100|0.31043117|  PASSED  
          sts_serial|   3|    100000|     100|0.52918756|  PASSED  
          sts_serial|   3|    100000|     100|0.90542292|  PASSED  
          sts_serial|   4|    100000|     100|0.14028712|  PASSED  
          sts_serial|   4|    100000|     100|0.41547961|  PASSED  
          sts_serial|   5|    100000|     100|0.01249698|  PASSED  
          sts_serial|   5|    100000|     100|0.73999914|  PASSED  
          sts_serial|   6|    100000|     100|0.25738137|  PASSED  
          sts_serial|   6|    100000|     100|0.76244373|  PASSED  
          sts_serial|   7|    100000|     100|0.36850587|  PASSED  
          sts_serial|   7|    100000|     100|0.94954237|  PASSED  
          sts_serial|   8|    100000|     100|0.70073540|  PASSED  
          sts_serial|   8|    100000|     100|0.41948051|  PASSED  
          sts_serial|   9|    100000|     100|0.83461485|  PASSED  
          sts_serial|   9|    100000|     100|0.42370394|  PASSED  
          sts_serial|  10|    100000|     100|0.56159803|  PASSED  
          sts_serial|  10|    100000|     100|0.40833723|  PASSED  
          sts_serial|  11|    100000|     100|0.38709683|  PASSED  
          sts_serial|  11|    100000|     100|0.64869263|  PASSED  
          sts_serial|  12|    100000|     100|0.84400311|  PASSED  
          sts_serial|  12|    100000|     100|0.77972411|  PASSED  
          sts_serial|  13|    100000|     100|0.17210977|  PASSED  
          sts_serial|  13|    100000|     100|0.55834969|  PASSED  
          sts_serial|  14|    100000|     100|0.83252065|  PASSED  
          sts_serial|  14|    100000|     100|0.08804565|  PASSED  
          sts_serial|  15|    100000|     100|0.24465797|  PASSED  
          sts_serial|  15|    100000|     100|0.58866498|  PASSED  
          sts_serial|  16|    100000|     100|0.93969946|  PASSED  
          sts_serial|  16|    100000|     100|0.41567412|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   1|    100000|     100|0.66764548|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   2|    100000|     100|0.20932731|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   3|    100000|     100|0.36919400|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   4|    100000|     100|0.86527064|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   5|    100000|     100|0.77504713|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   6|    100000|     100|0.35337504|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   7|    100000|     100|0.94029356|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   8|    100000|     100|0.71360097|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   9|    100000|     100|0.45973607|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|  10|    100000|     100|0.77609490|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|  11|    100000|     100|0.03818647|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|  12|    100000|     100|0.33934647|  PASSED  
# The file file_input_raw was rewound 1 times
rgb_minimum_distance|   2|     10000|    1000|0.97225058|  PASSED  
# The file file_input_raw was rewound 1 times
rgb_minimum_distance|   3|     10000|    1000|0.19945329|  PASSED  
# The file file_input_raw was rewound 1 times
rgb_minimum_distance|   4|     10000|    1000|0.66090525|  PASSED  
# The file file_input_raw was rewound 1 times
rgb_minimum_distance|   5|     10000|    1000|0.00500215|  PASSED  
# The file file_input_raw was rewound 1 times
    rgb_permutations|   2|    100000|     100|0.54687320|  PASSED  
# The file file_input_raw was rewound 1 times
    rgb_permutations|   3|    100000|     100|0.25395629|  PASSED  
# The file file_input_raw was rewound 1 times
    rgb_permutations|   4|    100000|     100|0.83638099|  PASSED  
# The file file_input_raw was rewound 1 times
    rgb_permutations|   5|    100000|     100|0.04477950|  PASSED  
# The file file_input_raw was rewound 1 times
      rgb_lagged_sum|   0|   1000000|     100|0.41093039|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   1|   1000000|     100|0.16935147|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   2|   1000000|     100|0.11315713|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   3|   1000000|     100|0.91605841|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   4|   1000000|     100|0.41347493|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   5|   1000000|     100|0.47380775|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   6|   1000000|     100|0.33507438|  PASSED  
# The file file_input_raw was rewound 3 times
      rgb_lagged_sum|   7|   1000000|     100|0.48331089|  PASSED  
# The file file_input_raw was rewound 3 times
      rgb_lagged_sum|   8|   1000000|     100|0.94314454|  PASSED  
# The file file_input_raw was rewound 3 times
      rgb_lagged_sum|   9|   1000000|     100|0.65851572|  PASSED  
# The file file_input_raw was rewound 4 times
      rgb_lagged_sum|  10|   1000000|     100|0.78068592|  PASSED  
# The file file_input_raw was rewound 4 times
      rgb_lagged_sum|  11|   1000000|     100|0.53664619|  PASSED  
# The file file_input_raw was rewound 5 times
      rgb_lagged_sum|  12|   1000000|     100|0.23552254|  PASSED  
# The file file_input_raw was rewound 5 times
      rgb_lagged_sum|  13|   1000000|     100|0.89326648|  PASSED  
# The file file_input_raw was rewound 6 times
      rgb_lagged_sum|  14|   1000000|     100|0.33235271|  PASSED  
# The file file_input_raw was rewound 6 times
      rgb_lagged_sum|  15|   1000000|     100|0.38603170|  PASSED  
# The file file_input_raw was rewound 7 times
      rgb_lagged_sum|  16|   1000000|     100|0.61132030|  PASSED  
# The file file_input_raw was rewound 8 times
      rgb_lagged_sum|  17|   1000000|     100|0.20579401|  PASSED  
# The file file_input_raw was rewound 8 times
      rgb_lagged_sum|  18|   1000000|     100|0.90542441|  PASSED  
# The file file_input_raw was rewound 9 times
      rgb_lagged_sum|  19|   1000000|     100|0.76841197|  PASSED  
# The file file_input_raw was rewound 10 times
      rgb_lagged_sum|  20|   1000000|     100|0.92823460|  PASSED  
# The file file_input_raw was rewound 11 times
      rgb_lagged_sum|  21|   1000000|     100|0.28825828|  PASSED  
# The file file_input_raw was rewound 12 times
      rgb_lagged_sum|  22|   1000000|     100|0.62991993|  PASSED  
# The file file_input_raw was rewound 13 times
      rgb_lagged_sum|  23|   1000000|     100|0.40999540|  PASSED  
# The file file_input_raw was rewound 14 times
      rgb_lagged_sum|  24|   1000000|     100|0.18022754|  PASSED  
# The file file_input_raw was rewound 14 times
      rgb_lagged_sum|  25|   1000000|     100|0.63245729|  PASSED  
# The file file_input_raw was rewound 15 times
      rgb_lagged_sum|  26|   1000000|     100|0.98889915|  PASSED  
# The file file_input_raw was rewound 17 times
      rgb_lagged_sum|  27|   1000000|     100|0.74986389|  PASSED  
# The file file_input_raw was rewound 18 times
      rgb_lagged_sum|  28|   1000000|     100|0.38576933|  PASSED  
# The file file_input_raw was rewound 19 times
      rgb_lagged_sum|  29|   1000000|     100|0.97102696|  PASSED  
# The file file_input_raw was rewound 20 times
      rgb_lagged_sum|  30|   1000000|     100|0.74433467|  PASSED  
# The file file_input_raw was rewound 21 times
      rgb_lagged_sum|  31|   1000000|     100|0.64394959|  PASSED  
# The file file_input_raw was rewound 22 times
      rgb_lagged_sum|  32|   1000000|     100|0.80502741|  PASSED  
# The file file_input_raw was rewound 22 times
     rgb_kstest_test|   0|     10000|    1000|0.75784628|  PASSED  
# The file file_input_raw was rewound 22 times
     dab_bytedistrib|   0|  51200000|       1|0.29960219|  PASSED  
# The file file_input_raw was rewound 22 times
             dab_dct| 256|     50000|       1|0.04060022|  PASSED  
# The file file_input_raw was rewound 22 times
Preparing to run test 207.  ntuple = 0
        dab_filltree|  32|  15000000|       1|0.53079441|  PASSED  
        dab_filltree|  32|  15000000|       1|0.81313879|  PASSED  
# The file file_input_raw was rewound 22 times
Preparing to run test 208.  ntuple = 0
       dab_filltree2|   0|   5000000|       1|0.21094819|  PASSED  
       dab_filltree2|   1|   5000000|       1|0.19193921|  PASSED  
# The file file_input_raw was rewound 22 times
Preparing to run test 209.  ntuple = 0
        dab_monobit2|  12|  65000000|       1|0.74619420|  PASSED  