#=============================================================================#
#            dieharder version 3.31.1 Copyright 2003 Robert G. Brown          #
#=============================================================================#
   rng_name    |           filename             |rands/second|
 file_input_raw|/home/lab/bytes/lcg/resources/lcg_mod2147483647_a48271_c0_10G.dat|  2.78e+07  |
#=============================================================================#
        test_name   |ntup| tsamples |psamples|  p-value |Assessment
#=============================================================================#
   diehard_birthdays|   0|       100|     100|0.00243097|   WEAK   
      diehard_operm5|   0|   1000000|     100|0.98026209|  PASSED  
  diehard_rank_32x32|   0|     40000|     100|0.73209651|  PASSED  
    diehard_rank_6x8|   0|    100000|     100|0.37732718|  PASSED  
   diehard_bitstream|   0|   2097152|     100|0.17933972|  PASSED  
        diehard_opso|   0|   2097152|     100|0.11302238|  PASSED  
        diehard_oqso|   0|   2097152|     100|0.44725454|  PASSED  
         diehard_dna|   0|   2097152|     100|0.02835500|  PASSED  
diehard_count_1s_str|   0|    256000|     100|0.57231258|  PASSED  
diehard_count_1s_byt|   0|    256000|     100|0.75182036|  PASSED  
 diehard_parking_lot|   0|     12000|     100|0.50331801|  PASSED  
    diehard_2dsphere|   2|      8000|     100|0.14624452|  PASSED  
    diehard_3dsphere|   3|      4000|     100|0.80774424|  PASSED  
     diehard_squeeze|   0|    100000|     100|0.74937274|  PASSED  
        diehard_sums|   0|       100|     100|0.02157232|  PASSED  
        diehard_runs|   0|    100000|     100|0.54240175|  PASSED  
        diehard_runs|   0|    100000|     100|0.64627138|  PASSED  
       diehard_craps|   0|    200000|     100|0.64002993|  PASSED  
       diehard_craps|   0|    200000|     100|0.66204680|  PASSED  
# The file file_input_raw was rewound 1 times
 marsaglia_tsang_gcd|   0|  10000000|     100|0.18883878|  PASSED  
 marsaglia_tsang_gcd|   0|  10000000|     100|0.66077396|  PASSED  
# The file file_input_raw was rewound 1 times
         sts_monobit|   1|    100000|     100|0.95675127|  PASSED  
# The file file_input_raw was rewound 1 times
            sts_runs|   2|    100000|     100|0.76749509|  PASSED  
# The file file_input_raw was rewound 1 times
          sts_serial|   1|    100000|     100|0.38738901|  PASSED  
          sts_serial|   2|    100000|     100|0.94913913|  PASSED  
          sts_serial|   3|    100000|     100|0.99621249|   WEAK   
          sts_serial|   3|    100000|     100|0.07795812|  PASSED  
          sts_serial|   4|    100000|     100|0.26103223|  PASSED  
          sts_serial|   4|    100000|     100|0.11403578|  PASSED  
          sts_serial|   5|    100000|     100|0.22609373|  PASSED  
          sts_serial|   5|    100000|     100|0.97466165|  PASSED  
          sts_serial|   6|    100000|     100|0.47308517|  PASSED  
          sts_serial|   6|    100000|     100|0.92021153|  PASSED  
          sts_serial|   7|    100000|     100|0.28575344|  PASSED  
          sts_serial|   7|    100000|     100|0.17316327|  PASSED  
          sts_serial|   8|    100000|     100|0.33328985|  PASSED  
          sts_serial|   8|    100000|     100|0.96285163|  PASSED  
          sts_serial|   9|    100000|     100|0.36771591|  PASSED  
          sts_serial|   9|    100000|     100|0.98077506|  PASSED  
          sts_serial|  10|    100000|     100|0.24329883|  PASSED  
          sts_serial|  10|    100000|     100|0.10021150|  PASSED  
          sts_serial|  11|    100000|     100|0.75007288|  PASSED  
          sts_serial|  11|    100000|     100|0.31772430|  PASSED  
          sts_serial|  12|    100000|     100|0.97582725|  PASSED  
          sts_serial|  12|    100000|     100|0.31089813|  PASSED  
          sts_serial|  13|    100000|     100|0.67099422|  PASSED  
          sts_serial|  13|    100000|     100|0.71057138|  PASSED  
          sts_serial|  14|    100000|     100|0.77018723|  PASSED  
          sts_serial|  14|    100000|     100|0.56322230|  PASSED  
          sts_serial|  15|    100000|     100|0.63984868|  PASSED  
          sts_serial|  15|    100000|     100|0.17247191|  PASSED  
          sts_serial|  16|    100000|     100|0.96539035|  PASSED  
          sts_serial|  16|    100000|     100|0.58335563|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   1|    100000|     100|0.55194961|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   2|    100000|     100|0.60903878|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   3|    100000|     100|0.62873509|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   4|    100000|     100|0.79873527|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   5|    100000|     100|0.07191607|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   6|    100000|     100|0.18354897|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   7|    100000|     100|0.48149775|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   8|    100000|     100|0.52671499|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|   9|    100000|     100|0.69553557|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|  10|    100000|     100|0.97357463|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|  11|    100000|     100|0.65433981|  PASSED  
# The file file_input_raw was rewound 1 times
         rgb_bitdist|  12|    100000|     100|0.56476195|  PASSED  
# The file file_input_raw was rewound 1 times
rgb_minimum_distance|   2|     10000|    1000|0.07042561|  PASSED  
# The file file_input_raw was rewound 1 times
rgb_minimum_distance|   3|     10000|    1000|0.09598606|  PASSED  
# The file file_input_raw was rewound 1 times
rgb_minimum_distance|   4|     10000|    1000|0.13587173|  PASSED  
# The file file_input_raw was rewound 1 times
rgb_minimum_distance|   5|     10000|    1000|0.00000003|  FAILED  
# The file file_input_raw was rewound 1 times
    rgb_permutations|   2|    100000|     100|0.43060553|  PASSED  
# The file file_input_raw was rewound 1 times
    rgb_permutations|   3|    100000|     100|0.70146043|  PASSED  
# The file file_input_raw was rewound 1 times
    rgb_permutations|   4|    100000|     100|0.99150460|  PASSED  
# The file file_input_raw was rewound 1 times
    rgb_permutations|   5|    100000|     100|0.12978651|  PASSED  
# The file file_input_raw was rewound 1 times
      rgb_lagged_sum|   0|   1000000|     100|0.99234652|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   1|   1000000|     100|0.90611414|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   2|   1000000|     100|0.20821811|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   3|   1000000|     100|0.84856928|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   4|   1000000|     100|0.53030803|  PASSED  
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   5|   1000000|     100|0.00321685|   WEAK   
# The file file_input_raw was rewound 2 times
      rgb_lagged_sum|   6|   1000000|     100|0.05183505|  PASSED  
# The file file_input_raw was rewound 3 times
      rgb_lagged_sum|   7|   1000000|     100|0.58037551|  PASSED  
# The file file_input_raw was rewound 3 times
      rgb_lagged_sum|   8|   1000000|     100|0.85605236|  PASSED  
# The file file_input_raw was rewound 3 times
      rgb_lagged_sum|   9|   1000000|     100|0.27987853|  PASSED  
# The file file_input_raw was rewound 4 times
      rgb_lagged_sum|  10|   1000000|     100|0.16749811|  PASSED  
# The file file_input_raw was rewound 4 times
      rgb_lagged_sum|  11|   1000000|     100|0.24317871|  PASSED  
# The file file_input_raw was rewound 5 times
      rgb_lagged_sum|  12|   1000000|     100|0.68245088|  PASSED  
# The file file_input_raw was rewound 5 times
      rgb_lagged_sum|  13|   1000000|     100|0.40573470|  PASSED  
# The file file_input_raw was rewound 6 times
      rgb_lagged_sum|  14|   1000000|     100|0.13484635|  PASSED  
# The file file_input_raw was rewound 6 times
      rgb_lagged_sum|  15|   1000000|     100|0.84146127|  PASSED  
# The file file_input_raw was rewound 7 times
      rgb_lagged_sum|  16|   1000000|     100|0.96411641|  PASSED  
# The file file_input_raw was rewound 8 times
      rgb_lagged_sum|  17|   1000000|     100|0.69588277|  PASSED  
# The file file_input_raw was rewound 8 times
      rgb_lagged_sum|  18|   1000000|     100|0.81915385|  PASSED  
# The file file_input_raw was rewound 9 times
      rgb_lagged_sum|  19|   1000000|     100|0.56854482|  PASSED  
# The file file_input_raw was rewound 10 times
      rgb_lagged_sum|  20|   1000000|     100|0.25615235|  PASSED  
# The file file_input_raw was rewound 11 times
      rgb_lagged_sum|  21|   1000000|     100|0.99577051|   WEAK   
# The file file_input_raw was rewound 12 times
      rgb_lagged_sum|  22|   1000000|     100|0.76680441|  PASSED  
# The file file_input_raw was rewound 13 times
      rgb_lagged_sum|  23|   1000000|     100|0.65068322|  PASSED  
# The file file_input_raw was rewound 14 times
      rgb_lagged_sum|  24|   1000000|     100|0.59309395|  PASSED  
# The file file_input_raw was rewound 14 times
      rgb_lagged_sum|  25|   1000000|     100|0.90904978|  PASSED  
# The file file_input_raw was rewound 15 times
      rgb_lagged_sum|  26|   1000000|     100|0.65590637|  PASSED  
# The file file_input_raw was rewound 17 times
      rgb_lagged_sum|  27|   1000000|     100|0.04068792|  PASSED  
# The file file_input_raw was rewound 18 times
      rgb_lagged_sum|  28|   1000000|     100|0.87019802|  PASSED  
# The file file_input_raw was rewound 19 times
      rgb_lagged_sum|  29|   1000000|     100|0.46575118|  PASSED  
# The file file_input_raw was rewound 20 times
      rgb_lagged_sum|  30|   1000000|     100|0.04111456|  PASSED  
# The file file_input_raw was rewound 21 times
      rgb_lagged_sum|  31|   1000000|     100|0.35947155|  PASSED  
# The file file_input_raw was rewound 22 times
      rgb_lagged_sum|  32|   1000000|     100|0.43081757|  PASSED  
# The file file_input_raw was rewound 22 times
     rgb_kstest_test|   0|     10000|    1000|0.85452867|  PASSED  
# The file file_input_raw was rewound 22 times
     dab_bytedistrib|   0|  51200000|       1|0.57819148|  PASSED  
# The file file_input_raw was rewound 22 times
             dab_dct| 256|     50000|       1|0.54874810|  PASSED  
# The file file_input_raw was rewound 22 times
Preparing to run test 207.  ntuple = 0
        dab_filltree|  32|  15000000|       1|0.59289818|  PASSED  
        dab_filltree|  32|  15000000|       1|0.68305676|  PASSED  
# The file file_input_raw was rewound 22 times
Preparing to run test 208.  ntuple = 0
       dab_filltree2|   0|   5000000|       1|0.23264777|  PASSED  
       dab_filltree2|   1|   5000000|       1|0.70908901|  PASSED  
# The file file_input_raw was rewound 22 times
Preparing to run test 209.  ntuple = 0
        dab_monobit2|  12|  65000000|       1|0.35659115|  PASSED  