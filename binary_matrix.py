def compute_rank(M, Q, matrix):
    m = min(M, Q)

    # FORWARD
    for i in range(m - 1):
        if matrix[i][i] == 1:
            perform_elementary_row_operations(True, i, M, Q, matrix)
        else:
            if find_unit_element_and_swap(True, i, M, Q, matrix) == 1:
                perform_elementary_row_operations(True, i, M, Q, matrix)

    # BACKWARD
    for i in range(m - 1, 0, -1):
        if matrix[i][i] == 1:
            perform_elementary_row_operations(False, i, M, Q, matrix)
        else:
            if find_unit_element_and_swap(False, i, M, Q, matrix) == 1:
                perform_elementary_row_operations(False, i, M, Q, matrix)

    rank = determine_rank(m, M, Q, matrix)
    return rank


def perform_elementary_row_operations(is_forward, i, M, Q, matrix):
    if is_forward:
        for j in range(i + 1, M):
            if matrix[j][i] == 1:
                for k in range(i, Q):
                    matrix[j][k] = (matrix[j][k] + matrix[i][k]) % 2
    else:
        for j in range(i - 1, -1, -1):
            if matrix[j][i] == 1:
                for k in range(Q):
                    matrix[j][k] = (matrix[j][k] + matrix[i][k]) % 2


def find_unit_element_and_swap(is_forward, i, M, Q, matrix):
    index = 0
    row_op = 0

    if is_forward:
        index = i + 1
        while index < M and matrix[index][i] == 0:
            index += 1
        if index < M:
            row_op = swap_rows(i, index, Q, matrix)
    else:
        index = i - 1
        while index >= 0 and matrix[index][i] == 0:
            index -= 1
        if index >= 0:
            row_op = swap_rows(i, index, Q, matrix)
    return row_op


def swap_rows(i, index, Q, matrix):
    temp = 0
    for p in range(Q):
        temp = matrix[i][p]
        matrix[i][p] = matrix[index][p]
        matrix[index][p] = temp
    return 1


def determine_rank(m, M, Q, matrix):
    rank = m

    for i in range(M):
        all_zeroes = True
        for j in range(Q):
            if matrix[i][j] == 1:
                all_zeroes = False
                break
        if all_zeroes:
            rank -= 1
    return rank


if __name__ == "__main__":
    import numpy as np

    matrix = np.array(
        [
            [1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 1],
            [1, 0, 1, 0, 1, 0],
            [0, 0, 1, 0, 1, 1],
            [0, 0, 0, 0, 1, 0],
        ]
    )
    expected = np.array(
        [
            [1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 1],
        ]
    )
    print(compute_rank(6, 6, matrix))
