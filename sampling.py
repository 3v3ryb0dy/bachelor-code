# Usage: sampling.py -p PATH -n TAKE_EVERY_NTH_BYTE
# Take an input file and output a new file with every n-th byte of the original file

import sys, getopt
import numpy as np

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:],"p:n:")
        print(opts)
    except getopt.GetoptError:
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-n':
            N = int(arg)
        elif opt == '-p':
            path = arg
    if N and path:
        pass
    else:
        sys.exit()



    a = np.fromfile(path, dtype=np.uint8)
    a = a[::N]

    newfile = path.split(".")[0] + f"_sampled_{N}.dat"
    a.tofile(newfile)

if __name__ == "__main__":
    main()