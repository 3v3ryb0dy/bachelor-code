# Bachelorarbeit Begleitcode

## Allgemeines

Dieses Repository enthält sämtlichen im Zusammenhang mit meiner Bachelorarbeit geschriebenen Code.\
Thema der Bachelorarbeit ist die Evaluierung verschiedener klassicher und quantenmechanischer Zufallszahlquellen.\
Dafür werden verschiedene Arten von Zufallszahlengeneratoren mit den Testsuiten NIST SP 800-22, Dieharder und ENT getestet.

*Für die Nutzung werden Python in einer aktuellen Version sowie die Bibliotheken numpy, scipy, pandas und matplotlib benötigt*

Die Bestandteile sind:

- `linear_congruential_generator.ipynb`
  - Linearer Kongruenzgenerator zur Erzeugung von Pseudozufallszahlen
- `qiskit.ipynb`
  - Nutzung von IBM Quantencomputern als Zufallszahlengenerator
- `monte_carlo_pi.ipynb`
  - Monte-Carlo-Methode zur Bestimmung von Pi
- `bitmap.ipynb`
  - Bitmaps aus generierten Zufallszahlen
- `statistical_tests.ipynb` (+ `binary_matrix.py`)
  - Teile der NIST SP 800-22 Suite zum Testen von Zufallszahlengeneratoren
- `evaluation.ipynb` (+ `sampling.py`)
  - Verarbeitung und Aufbereitung der Ergebnisse

Weiterhin die Ordner:

- `resources`
  - Einige 1MB große Dateien mit Zufallszahlen
- `results`
  - Sämtliche Ergebnisse der Tests pro Testsuite

## Durchgeführte Tests

Die folgenden Tests wurden dabei für die folgenden Generatoren durchgeführt.

| Generator                                   |  ENT  | NIST  | NIST (sampled) | Dieharder |
| ------------------------------------------- | :---: | :---: | :------------: | :-------: |
| lcg_mod16777216_a189_c1                     |   ✅   |   ✅   |       ❌        |     ✅     |
| lcg_mod16777216_a315_c1                     |   ✅   |   ✅   |       ❌        |     ✅     |
| lcg_mod2147483647_a16807_c0                 |   ✅   |   ✅   |       ✅        |     ✅     |
| lcg_mod2147483647_a48271_c0                 |   ✅   |   ✅   |       ✅        |     ✅     |
| urandom                                     |   ✅   |   ✅   |       ✅        |     ✅     |
| urandom_empty                               |   ✅   |   ✅   |       ✅        |     ✅     |
| IDQ                                         |   ✅   |   ✅   |       ✅        |     ✅     |
| Quintessence Labs (raw)                     |   ✅   |   ✅   |       ❌        |     ✅     |
| Quintessence Labs (conditioned)             |   ✅   |   ✅   |       ✅        |     ✅     |
| Quintessence Labs (conditioned + Quantropi) |   ✅   |   ✅   |       ✅        |     ✅     |
| IBMQ                     ✅                  |   ✅   |   ✅   |       ✅        |     ✅     |

✅ - durchgeführt\
❌ - nicht durchgeführt

(sampled) - Von den ursprünglichen Dateien wurde nur jedes 50. Byte untersucht
